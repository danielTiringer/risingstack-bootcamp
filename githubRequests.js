const axios = require("axios")

const token = process.env.GITHUB_ACCESS_TOKEN;

let searchRepositories = async function(query = {}) {
    let queryString;
    if (Object.keys(query) == 0) {
        queryString = '';
    } else {
        queryString = Object.values(query).join(', ');
    }

    return await axios({
        url: 'https://api.github.com/graphql',
        method: 'post',
        headers: {
            Authorization: `token ${token}`
        },
        data: {
            query: `{
                search(type: REPOSITORY, query: "${queryString}", first: 20) {
                    repositoryCount
                    nodes {
                        ... on Repository {
                            id
                            name
                            owner{
                                login
                            }
                            languages(first: 10) {
                            nodes {
                                name
                                color

                            }
                            totalSize
                            }
                        }
                    }
                }
            }`,
            variables: {
                "queryString": query
            }
        }
    });
}

module.exports.searchRepositories = searchRepositories;
