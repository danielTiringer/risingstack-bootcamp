# RisingStack bootcamp

My take on the [RisingStack bootcamp](https://github.com/RisingStack/risingstack-bootcamp).

## Differences compared to the stories:

- Developing with Docker containers instead of installing software on bare metal
- Utilizing CI/CD tools
- Using Yarn instead of NPM
