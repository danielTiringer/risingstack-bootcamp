const Koa = require('koa');
const KoaRouter = require('@koa/router');

const server = new Koa();
const router = new KoaRouter();

const PORT = process.env.SERVER_PORT;

router.get('/hello', ctx => {
    ctx.body = 'Hello Node.js!';
});

server.use(router.routes()).use(router.allowedMethods());

module.exports = server.listen(PORT, () => {
    console.log('Koa server started on port ' + PORT);
});

