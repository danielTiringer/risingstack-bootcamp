const tableName = 'user';

function up(knex) {
    return knex.schema.createTable(tableName, (table) => {
        table.integer('id').unsigned().primary();
        table.string('login');
        table.string('avatar_url');
        table.string('html_url');
        table.string('type');
    })
}

function down(knex) {
    return knex.schema.dropTableIfExists(tableName);
}

module.exports = {
    up,
    down
}
