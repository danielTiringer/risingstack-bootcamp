const tableName = 'contribution';

function up(knex) {
    return knex.schema.createTable(tableName, (table) => {
        table.integer('user').unsigned();
        table.foreign('user').references('user.id');
        table.integer('repository').unsigned();
        table.foreign('repository').references('repository.id');
        table.integer('line_count');
        table.primary(['user', 'repository']);
    })
}

function down(knex) {
    return knex.schema.dropTableIfExists(tableName);
}

module.exports = {
    up,
    down
}
