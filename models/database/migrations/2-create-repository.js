const tableName = 'repository';

function up(knex) {
    return knex.schema.createTable(tableName, (table) => {
        table.integer('id').unsigned().primary();
        table.integer('owner');
        table.string('full_name');
        table.string('description');
        table.string('html_url');
        table.string('language');
        table.integer('stargazers_count');
    })
}

function down(knex) {
    return knex.schema.dropTableIfExists(tableName);
}

module.exports = {
    up,
    down
}
