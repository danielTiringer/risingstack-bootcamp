const knex = require('knex');
const path = require('path');

const db = knex({
    client: 'pg',
    connection: {
        host: 'db',
        database: process.env.POSTGRES_DB,
        user: process.env.POSTGRES_USER,
        password: process.env.POSTGRES_PASSWORD
    },
    migrations: {
        directory: path.join(__dirname, './migrations'),
        tableName: 'migrations'
    }
});

module.exports = db;
