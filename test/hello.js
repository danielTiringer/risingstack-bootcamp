const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');

chai.should();

chai.use(chaiHttp);

describe('Hello endpoint', function() {
    describe('GET /hello', function() {
        it('Should return Hello Node.js!', function(done) {
            chai.request(server)
                .get('/hello')
                .end((_, response) => {
                    response.should.have.status(200);
                    response.should.have.property('text').to.equal('Hello Node.js!');
                    done();
                })
        })
    })
})
