const chai = require('chai');
const axios = require('axios');
const MockAdapter = require('axios-mock-adapter');
const githubRequests = require('../githubRequests');

chai.should();

describe('Github GraphQL queries', function() {
    let mock;

    before(function() {
        mock = new MockAdapter(axios);
    });

    afterEach(function() {
        mock.reset();
    });

    after(function() {
        mock.restore();
    });

    describe('searchRepositories with empty object', function() {
        it('Should return a Promise and have an empty queryString', function(done) {
            mock.onPost('https://api.github.com/graphql')
                .reply(function() {
                    return new Promise();
                });

            const response = githubRequests.searchRepositories({});
            response.should.be.an.instanceof(Promise);
            response.then(data => {
                data.config.headers.should.have.property('Authorization');
                data.config.headers.should.have.string('token ');
                data.config.data.should.have.string('query: \\"\\"');
            });
            done();
        })
    })

    describe('searchRepositories with non-empty object', function() {
        it('Should return a Promise and have an assembled queryString', function(done) {
            mock.onPost('https://api.github.com/graphql')
                .reply(function() {
                    return new Promise();
                });

            const response = githubRequests.searchRepositories({
                q: 'language:javascript',
                f: 'framework:koa'
            });
            response.should.be.an.instanceof(Promise);
            response.then(data => {
                data.config.headers.should.not.have.property('Authorization');
                data.config.headers.should.have.string('token ');
                data.config.data.should.have.string('query: \\"language:javascript, framework:koa\\"');
            });
            done();
        })
    })
})
