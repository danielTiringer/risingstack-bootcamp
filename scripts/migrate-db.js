const knex = require('../models/database')

const args = process.argv.slice(2)
if (args.includes('--latest')) {
    knex.migrate.latest()
      .then(() => {
          console.log('Migrations successfully completed');
          process.exit(0);
      })
      .catch(err => {
          console.error(err);
          process.exit(1);
      });
}

if (args.includes('--down')) {
    knex.migrate.rollback()
      .then(() => {
          console.log('Migrations successfully rolled back');
          process.exit(0);
      })
      .catch(err => {
          console.error(err);
          process.exit(1);
      });
}

if (args.includes('--list')) {
    knex.migrate.list()
        .then(() => {
            console.log('list')
        });
    process.exit(0);
}
